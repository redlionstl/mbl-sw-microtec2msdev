/*****************************************************************************
 * Copyright(c) 2007 N-Tron Corp.  All rights reserved.                      *
 *****************************************************************************
 *
 *   Creator: Dan Tappe
 *   Created: 7/12/2007  11:41
 *
 * $Revision: 13 $
 *     $Date: 11/16/11 3:29p $
 *   $Author: Rposer $
 *  $Archive: /Utilities/Microtec2MsDev/Microtec2MsDev.cpp $
 *
 * Description:
 * ------------
 * Convert FROM:
 * (W) C9999-D; ".\Application/Globals.h", line 109 pos 1; TODO: Move default user stuff someplace else
 * (E) C0065; "Application\Application.cpp", line 69 pos 1; expected a ";"
 *   void Application_Initialize
 *   ^
 * (W) C0012-D; "Application\Application.cpp", line 137 pos 39; parsing restarts here after previous syntax error
 *      NU_Change_Preemption(NU_NO_PREEMPT);
 *
 * (I) C0228-D; ".\Igmp/IgmpLocal.h", line 118 pos 24; trailing comma is nonstandard
 * BER_NO_MGMT_FRAME = 15,
 *                       ^
 * (F) C0005; ".\Port/Port.h", line 35 pos 34; could not open source file "Application/Globals2.h"
 *   #include "Application/Globals2.h"
 *                                    ^
 * asmppc -o o/d/int.obj -p 860  -DNTRON_ROM=1 -DNTRON_MODS=1 -D_DEBUG -I ./Inc  Src/int.s
 * < asmppc: Src/int.s >
 * "Src/int.s", line 2317; (E) #A0553-D Duplicate label or name.
 * Errors: 1, Warnings: 0
 * make: *** [o/d/int.obj] Error 1
 *
 * asmppc -o obj-ram/int.obj -DNTRON_BOOT_LOADER=1 -DNTRON_ROM=0 -DNTRON_MODS=1 -DNU_ERROR_STRING -DNU_ENABLE_HISTORY -DNU_ENABLE_STACK_CHECK -p 860 -g  int.s
 * < asmppc: w:\700Series\Projects\Nucleus\plus_specific\int.s >
 * "w:\700Series\Projects\Nucleus\plus_specific\int.s", line 596; (E) #A0502-D Operator expected.
 * Errors: 1, Warnings: 0
 * make: *** [obj-ram/int.obj] Error 1
 * 
 * Convert TO:
 * w:\Utilities\Microtec2MsDev\Microtec2MsDev.cpp(109) : error C2065: 'x' : undeclared identifier
 * 
 * fputs("(W) C9999-D; \"Application\\Application.cpp\", line 74 pos 1; TODO: Look into DHCP Option 82 (Port based IP assignment)\r\n", l_out);
 * fputs("Application\\Application.cpp(74) : warning C9999-D: TODO: Look into DHCP Option 82 (Port based IP assignment)\r\n", l_out);
 *
 *****************************************************************************
*/

#include <stdio.h>
#include <string.h>
#include "stdafx.h"

//#define DEBUG_LINE_PARSING	// Uncomment out this line to enable line pasing debug.

//-----------------------//
//--- N-Tron Includes ---//
//-----------------------//

//----------------------------------------------------------------------------
//--- Macros -----------------------------------------------------------------
//----------------------------------------------------------------------------
#define _MAX_LINE_LEN	1024
#define _SEPS_ASM		"\";"
#define _SEPS			"\":;"
#define _FROM			" from "
#define _FATAL			"(F)"
#define _ERROR			"(E)"
#define _WARNING		"(W)"
#define _INFO			"(I)"
#define _OPENPAREN		'('
#define _QUOTEMARK		'\"'
#define _LINE_IN		", line "
#define _ERROR_OUT		"fatal error"
#define _WARNING_OUT	"warning"
#define _INFO_OUT		"informational"
#define _LIB_MISSING	"#A0107-D"
#define _INFO_STR		"(I) C9999-D"

#define _EXCEPTION_1	"at end of source: "

//----------------------------------------------------------------------------
//--- Types ------------------------------------------------------------------
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
//--- Globals ----------------------------------------------------------------
//----------------------------------------------------------------------------
static char _line[_MAX_LINE_LEN];
static char _tmp[_MAX_LINE_LEN];

//----------------------------------------------------------------------------
//--- Functions --------------------------------------------------------------
//----------------------------------------------------------------------------

int _isNum (char *x_str)
{
	size_t l_i;

	// ~FOR 
	for (l_i=0;l_i<strlen(x_str);l_i++)
	{
		//  ~IF 
		if ( !((x_str[l_i] >= '0') && (x_str[l_i] <= '9')) )
		{ //  ~THEN 
			return 0;
		} // ~ENDIF 
	} // ~ENDFOR 

	return 1;
}

void strcatNum (char *line, char *x_str)
{
	// Don't output the (F), (I), (E), (W), etc
	if (x_str[0] == '(')
		strcat (line, &x_str[4]);
	else
		strcat (line, x_str);
}

int _tmain (int x_argc, char *x_argv[ ] )
{
	FILE *	l_in,	*l_out;
	char *	l_file;
	char *	l_num;
	char *	l_end;
	int		l_ret;
	char *	szErrType;
	char *	szErrNum;
	char *	szPos;
	char *	szStart;
	int		nException		= 0;
	int		nErrors			= 0;
	int		nWarnings		= 0;
	int		nInformational	= 0;
	int		show_libs		= 0;
	int		show_info		= 1;
	int		i;
	size_t	nLen;
	bool	bIsAsm			= false;

	if (x_argc > 1)
	{
		for(i=1;i<x_argc;i++) 
		{
			if (strcmp(x_argv[i],"-l")==0) 
			{
				show_libs = 1;
			}
			else if (strcmp(x_argv[i],"-i")==0)
			{
				show_info = 0;
			}
			else
			{
				fprintf (stderr, "only arguments are '-l' and '-i', uses stdin, stdout\n");
				return 1;
			}
		}
	}

	l_in	= stdin;
	l_out	= stdout;

	l_ret = 0;
	while( fgets(_line,_MAX_LINE_LEN,l_in) )
	{
		//printf ("*%s", _line);
		strcpy (_tmp, _line);

		szStart = _tmp;

		// Handle Start of Exception 1
		if (strncmp(szStart,_EXCEPTION_1,strlen(_EXCEPTION_1))==0)
		{
			nException = 1;
			szStart += strlen(_EXCEPTION_1);
			strcpy (_tmp, szStart);
			szStart = _tmp;
		}

		while (isspace(*szStart))
			szStart++;

		if (*szStart != _OPENPAREN && *szStart != _QUOTEMARK)
		{
			goto OutputLine;
		}

		if ( strncmp(szStart, _ERROR, strlen(_ERROR)) == 0)
		{
			szErrType = _ERROR_OUT;
			nErrors++;
		}
		else if ( strncmp(szStart, _FATAL, strlen(_FATAL)) == 0)
		{
			szErrType = _ERROR_OUT;
			nErrors++;
		}
		else if  ( strncmp(szStart, _WARNING, strlen(_WARNING)) == 0)
		{
			szErrType = _WARNING_OUT;
			nWarnings++;
		}
		else if  ( strncmp(szStart, _INFO, strlen(_INFO)) == 0)
		{
			if ((show_info)&&(strncmp(szStart, _INFO_STR, strlen(_INFO_STR))))
			{
				szErrType = _WARNING_OUT;
				nWarnings++;
			}
			else
			{
				szErrType = _INFO_OUT;
				nInformational++;
			}
		}
		else if (*szStart == '\"')
		{
			// Assembly Error/Warning/Info
			bIsAsm = true;

			if ( strstr(szStart, _ERROR) != 0)
			{
				szErrType = _ERROR_OUT;
				nErrors++;
			}
			else if  ( strstr(szStart, _WARNING) != 0)
			{
				szErrType = _WARNING_OUT;
				nWarnings++;
			}
			else
				goto OutputLine;
		}
		else
			goto OutputLine;

		//
		// If we are here, then this should be an error or warning line
		//
		if (bIsAsm)
		{
			// Asm
			l_file = strtok( _tmp, _SEPS_ASM);
			if ( l_file == NULL )
				goto OutputLine;
			//printf ("*File: %s\n", l_file);
			l_num = strtok( NULL, _SEPS_ASM);
			//printf ("*l_num: %s\n", l_num);
			szErrNum = strtok( NULL, _SEPS_ASM);
			while (isspace(*szErrNum))
				szErrNum++;
			l_end = szErrNum;
			while ( *l_end != '\0' && ! isspace(*l_end))
				l_end++;
			while ( isspace(*l_end))
				l_end++;
			while ( *l_end != '\0' && ! isspace(*l_end))
				l_end++;
			*l_end = '\0';
			l_end++;
			//printf ("*szErrNum: %s\n", szErrNum);
			//printf ("*l_end: %s\n", l_end);
		}
		else
		{
			// C/C++
			szErrNum = strtok( _tmp, _SEPS);
			//  ~IF 
			if ( szErrNum == NULL )
				goto OutputLine;
			//printf ("*ErrNum: %s\n", szErrNum);

			// Handle End Of Exception 1
			if (nException == 1) 
			{
				l_end = strtok(NULL, "");
				nException = 0;
				*_line = '\0';
				strcat(_line, "Unknown(0) : ");
				strcat(_line, szErrType);
				strcat(_line, " ");
				strcatNum( _line, szErrNum);
				strcat(_line, ": ");
				strcat(_line, _EXCEPTION_1);
				strcat(_line, l_end);
				goto OutputLine;
			}

			l_file = strtok( NULL, _SEPS);
			l_file = strtok( NULL, _SEPS);
			if ( l_file == NULL ) 
			{
				if ((szErrType == _WARNING_OUT)||(szErrType == _ERROR_OUT)) 
				{
					szErrNum = strtok( _tmp, " ");
					szErrNum = strtok( NULL, " ");
					if ((!show_libs)&&(strcmp(szErrNum, _LIB_MISSING)==0)) 
					{
						goto OutputLine;
					}
					//printf ("*ErrNum: %s\n", szErrNum);
					l_end = strtok(NULL, "");

					*_line = '\0';
					strcat (_line, " \n");		// Add an extra line
					strcat (_line, ": ");
					strcat (_line, szErrType);
					strcat (_line, " ");
					strcatNum (_line, szErrNum);
					strcat (_line, ": ");

					if (l_end != NULL)
					{
						//printf("*END: %s", l_end);
						strcat (_line, l_end);
					}
					else
					{
						//printf("*Add EOL");
						strcat (_line, "\n");
					}
					strcat (_line, " \n");		// Add an extra line

				}
				goto OutputLine;
			}
			//printf ("*file: %s\n", l_file);

			l_num = strtok( NULL, _SEPS);
			l_end = strtok(NULL, "");
		}

		if ( l_num == NULL )
			goto OutputLine;
		if (strncmp(l_num, _LINE_IN, strlen(_LINE_IN)) != 0)
			goto OutputLine;
		l_num += strlen(_LINE_IN);
		//printf ("*num: %s\n", l_num);

		if (bIsAsm)
		{
			szPos = "1";
		}
		else
		{
			l_num = strtok(l_num, " ");
			szPos = strtok(NULL, ";");
			if ( szPos == NULL )
				goto OutputLine;
		}

		//  ~IF number
		if (_isNum(l_num) != 1)
			goto OutputLine;

		//printf ("*is num\n");

		*_line = '\0';
		if (! bIsAsm)
		{
			strcat (_line, " \n");		// Add an extra line
		}
		strcat (_line, l_file);
		strcat (_line, "(");
		strcat (_line, l_num);
		strcat (_line, ") : ");
		strcat (_line, szErrType);
		strcat (_line, " ");
		strcatNum( _line, szErrNum);
		strcat (_line, ": ");

		if (! bIsAsm)
		{
			strcat (_line, szPos);
			strcat (_line, ": ");
		}

		if (l_end != NULL)
		{
			//printf("*END: %s", l_end);
			strcat (_line, l_end);
		}
		else
		{
			//printf("*Add EOL");
			strcat (_line, "\n");
		}

		if (! bIsAsm)
		{
			strcat (_line, " \n");		// Add an extra line
		}

OutputLine:
		fputs(_line,l_out);
		fflush(l_out);			// Force each output line to screen
	}

	//
	// Build and output trailer
	//
	sprintf(_line, "###  %d Errors, %d Warnings, %d Informational  #######################################\n", nErrors, nWarnings, nInformational);
	nLen = strlen(_line) - 1;
	_tmp[nLen] = '\0';
	while(nLen--)
		_tmp[nLen] = '#';
	strcat(_tmp, "\n");

	fputs(" \n", l_out);
	fputs(_tmp, l_out);
	fputs(_line, l_out);
	fputs(_tmp, l_out);
	fputs(" \n", l_out);
	fflush(l_out);

	return nErrors;		// Return non-zero if we have any errors
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// $History: Microtec2MsDev.cpp $ 
// 
// *****************  Version 13  *****************
// User: Rposer       Date: 11/16/11   Time: 3:29p
// Updated in $/Utilities/Microtec2MsDev
// Informational "Warnings" were not counting as warnings in the final
// build count.
// 
// *****************  Version 12  *****************
// User: Rposer       Date: 11/16/11   Time: 9:33a
// Updated in $/Utilities/Microtec2MsDev
// Fixed Formatting Problem which prevented VS2010 from reporting errors
// and warnings
// 
// *****************  Version 11  *****************
// User: Rposer       Date: 8/27/08    Time: 5:16p
// Updated in $/Utilities/Microtec2MsDev
// Fixed exception to MicroTec compiler output which could cause certain
// errors to NOT be reported.
// 
// *****************  Version 10  *****************
// User: DTappe       Date: 9/26/07    Time: 2:00p
// Updated in $/Utilities/Microtec2MsDev
// Fixed asm parsing to handle file names with colons.
// 
// *****************  Version 9  *****************
// User: DTappe       Date: 9/20/07    Time: 11:50a
// Updated in $/Utilities/Microtec2MsDev
// Added support for assembly errors and warnings.
// 
// *****************  Version 8  *****************
// User: Rposer       Date: 8/14/07    Time: 2:18p
// Updated in $/Utilities/Microtec2MsDev
// Fixed Problem With Error vs Informational vs Warning
// 
// *****************  Version 7  *****************
// User: Rposer       Date: 8/14/07    Time: 11:04a
// Updated in $/Utilities/Microtec2MsDev
// Added functionality for further Errors/Warnings and Informational
// Messages
// 
// *****************  Version 6  *****************
// User: Rposer       Date: 8/06/07    Time: 10:02a
// Updated in $/Utilities/Microtec2MsDev
// Fixed to properly handle Fatal Errors and Non-Lib Warnings.  Now shows
// both in the Task List after compilation and final Project compilation
// shows properly.
// 
// *****************  Version 5  *****************
// User: DTappe       Date: 7/24/07    Time: 10:30a
// Updated in $/Utilities/Microtec2MsDev
// Added support for fatal errors.
// 
// *****************  Version 4  *****************
// User: DTappe       Date: 7/13/07    Time: 1:22p
// Updated in $/Utilities/Microtec2MsDev
// Added error counters
// 
// *****************  Version 3  *****************
// User: DTappe       Date: 7/12/07    Time: 5:13p
// Updated in $/Utilities/Microtec2MsDev
// 
// *****************  Version 2  *****************
// User: DTappe       Date: 7/12/07    Time: 11:46a
// Updated in $/Utilities/Microtec2MsDev
// Modified to work with Microtec compiler.
